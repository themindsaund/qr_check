# qr_check

Проект для развития спортивного ориентирования.
Будет разработано два клиентских приложения:<br>
 - для организаторов мероприятия [Event organizer](https://gitlab.com/themindsaund/qr_check/-/tree/main/event_organizer?ref_type=heads#event_organizer)<br>
 - для участников мероприятия [Sportsman](https://gitlab.com/themindsaund/qr_check/-/tree/main/sportsman?ref_type=heads#sportsman)

## Помощь с проектом

- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)
