import '../../domain/model/checkpoint.dart';
import '../api/model/api_checkpoint.dart';

class CheckpointMapper {
  static Checkpoint _fromApi(ApiCheckpoint checkpoint) {
    return Checkpoint(
      info: checkpoint.info.toString(),
      time: DateTime.parse(checkpoint.time),
    );
  }

  static List<Checkpoint> listFromApi(List<ApiCheckpoint> dataList) {
    var split = <Checkpoint>[];
    for (var data in dataList) {
      split.add(_fromApi(data));
    }
    return split;
  }

  static Checkpoint fromApi(ApiCheckpoint checkpoint) {
    return _fromApi(checkpoint);
  }

  static Map toApi(Checkpoint checkpoint) => {
        'info': checkpoint.info,
        'time': checkpoint.time.toIso8601String(),
      };
}
