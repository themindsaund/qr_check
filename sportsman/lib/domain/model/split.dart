import 'checkpoint.dart';

class Split {
  DateTime? startTime;
  DateTime? finishTime;
  List<Checkpoint>? marks;

  Split({
    this.startTime,
    this.finishTime,
  });
}
