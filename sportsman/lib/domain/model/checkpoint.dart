class Checkpoint {
  final String info;
  final DateTime time;

  Checkpoint({
    required this.info,
    required this.time,
  });
}
