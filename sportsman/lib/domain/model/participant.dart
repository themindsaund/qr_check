import 'split.dart';

class Participant {
  final String name;
  final String surname;
  final int chip;
  Split? split;

  Participant({
    required this.name,
    required this.surname,
    required this.chip,
  });
}
