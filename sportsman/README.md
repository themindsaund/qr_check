# sportsman

Этот клиент будет создан для спортсменов или просто участников мероприятий.

## Features

 - Регистрирует пользователя спомощью специального QR<br>
        (~~учитывая группу пользователя и его трассу~~)
 - Сканирует специальные QR:<br>
        - старта<br>
        - финиша<br>
        - контрольного пункта<br>
        - очистки
 - Локально хранит данные пользователя и его отметки 
 - Отображает результат пользователя ввиде рядя специальных QR.
 - ~~Нереализованный функционал~~

# Getting Started

Для запуска приложения выполните:

    flutter pub get
    flutter packages pub run build_runner build
    flutter run